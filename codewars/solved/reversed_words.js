// https://www.codewars.com/kata/5259b20d6021e9e14c0010d4/train/javascript

'use strict';

function reverseWords(str) {
    let words = str.split(' ')
    let ret = []

    for (let w of words) {
        let w_reversed = ''
        for (let i = w.length - 1; i >= 0; i--) {
            w_reversed += w[i]
        }
        ret.push(w_reversed)
    }

    return ret.join(' ')
}

console.log(reverseWords('double spaces  asdf'))