# https://www.codewars.com/kata/550f22f4d758534c1100025a

opposite_dirs = {
    'NORTH': 'SOUTH',
    'SOUTH': 'NORTH',
    'EAST':  'WEST',
    'WEST':  'EAST'
}


def reduceDirs(arr):
    for i in range(len(arr) - 1):
        if opposite_dirs[arr[i]] == arr[i + 1]:
            del arr[i + 1]
            del arr[i]
            return True
    return False


def dirReduc(arr):
    while reduceDirs(arr):
        pass
    
    return arr


a = ["NORTH", "SOUTH", "SOUTH", "EAST", "WEST", "NORTH", "WEST"]
print(dirReduc(a))
