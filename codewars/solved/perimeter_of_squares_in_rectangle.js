// https://www.codewars.com/kata/559a28007caad2ac4e000083

'use strict';

function perimeter(n) {
    console.assert(Number.isInteger(n) && n >= 0, 'n has to be an integer >= 0')

    let val_prev = 1
    let val_prev_prev = 0
    let fib_sum = 1

    for (let i = 0; i < n; i++) {
        let val = val_prev + val_prev_prev
        fib_sum += val
        val_prev_prev = val_prev
        val_prev = val
    }

    return fib_sum * 4
}

console.log(perimeter(0))
console.log(perimeter(5))