"""
Solution for the ludicrous coloured triangles kata on codewars.
What didn't work:
    - colour skipping (reduce multiple occurrences of the same colour)
        examples: RRBBBB, BGRRRR
"""
import random
import time
from typing import List, Callable, Dict, Any, Tuple
import itertools


def generate_starting_row(length: int) -> str:
    base_letters = 'RGB'
    r = ''
    for i in range(length):
        r += random.choice(base_letters)
    return r


solutions_simple = {
    'RR': 'R', 'GG': 'G', 'BB': 'B',
    'RG': 'B', 'GB': 'R', 'BR': 'G',
    'GR': 'B', 'BG': 'R', 'RB': 'G',
}


def solution_simple(seq: str) -> str:
    while len(seq) > 1:
        tmp = ''
        for i in range(1, len(seq)):
            tmp += solutions_simple[seq[i - 1:i + 1]]
        seq = tmp
    return seq


def build_clusters(size: int, increasing_size=True, alphabet='RGB') -> Dict[str, str]:
    clusters = {}
    sizes = range(1, size + 1) if increasing_size else range(size + 1, size + 2)
    for s in sizes:
        raw_strings = itertools.product(alphabet, repeat=s)
        r: Tuple[str]
        for r in raw_strings:
            joined = ''.join(r)
            clusters[joined] = solution_simple(joined)
    return clusters


def solution_prebuilt_clusters(seq: str, cluster_length=4) -> str:
    # TODO: remove this, doesn't work
    clusters = build_clusters(cluster_length)
    
    while len(seq) > 1:
        tmp = ''
        for i in range(0, len(seq), cluster_length):
            tmp += clusters[seq[i:i + cluster_length]]
        seq = tmp
    return seq


def evaluate_algorithm(sets: List[str], name: str, func: Callable, validate=False) -> float:
    time_start = time.process_time()
    i = 0
    print(f'{name} starting')
    for s in sets:
        print(f'result {i}: ', end='')
        result = func(s)
        print(f'{result}')
        if validate:
            result_trusted = solution_simple(s)
            if result != result_trusted:
                raise Exception(f'validation of result {i} failed. {result} seen, should be {result_trusted}')
        i += 1
    time_duration = time.process_time() - time_start
    print(f'{name} finished took {time_duration}s')
    return time_duration


if __name__ == '__main__':
    iterations = 10
    length = 5
    
    print(f'{iterations} iterations of length {length}')
    
    print(f'generating initial sets')
    sets = []
    for i in range(iterations):
        s = generate_starting_row(length)
        sets.append(s)
        print(f'set {i}: {s}')
    
    duration_naive = evaluate_algorithm(sets, 'naive solution', solution_simple, False)
    
    duration_prebuilt = evaluate_algorithm(sets, 'prebuilt clusters', solution_prebuilt_clusters, True)
    
    print(f'naive_solution: {duration_naive}s')
    print(f'solution_prebuilt_clusters: {duration_prebuilt}s')
