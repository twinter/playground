# https://www.codewars.com/kata/5a667236145c462103000091/python
#
# short summary: reorder a sequence of numbers of 1..n so that the sum of two
# consecutive numbers is a square number. Return False if that's not possible.

# Notes:
# in this problem we are essentially looking for a hamiltonian path (see https://en.wikipedia.org/wiki/Hamiltonian_path)
#  in an undirected graph. Two nodes in this graph are connected when the value of both nodes added together is equal
#  to a perfect square (i.e. 1 and 3 are connected since 1+3=4). The problem is that the search for a Hamiltonian path
#  is an NP-complete problem.


import functools
import logging
import math
import random
import time
from collections.abc import Collection
from typing import List, Dict, Iterator

Sequence = List[int]
SequenceList = List[Sequence]


class NotCombinableError(ValueError):
    pass


def timed(name):
    def decorator(func):
        @functools.wraps(func)
        def wrapper_timer(*args, **kwargs):
            tic = time.perf_counter()
            value = func(*args, **kwargs)
            toc = time.perf_counter()
            elapsed_time = toc - tic
            logging.info(f"{name} completed, time: {elapsed_time:0.4f} seconds")
            return value
        
        return wrapper_timer
    
    return decorator


class Graph:
    """
    Class representing a graph between pairs of natural numbers smaller than a specific limit whose sum
    is a perfect square. It allows for the removal of connections and vertices to the graph to aid different
    search algorithms as well as the reset of all modifications.
    """
    
    # TODO: move these into init, these should not be class attributes
    _connections: Dict[int, List[int] | None] = {}
    _connections_unmodified: Dict[int, List[int] | None] = {}
    
    def __init__(self, n: int):
        self.expand(n)
    
    @property
    def size(self):
        """
        The size of the unmodified graph as well as the largest number included in the graph.
        """
        return len(self._connections_unmodified)
    
    def _connection_add(self, a: int, b: int):
        """
        Add a connection permanently. This connection will be restored when calling reset().
        :param a:
        :param b:
        :return:
        """
        
        assert a not in self._connections_unmodified[b]
        assert b not in self._connections_unmodified[a]
        
        self._connections_unmodified[a].append(b)
        self._connections_unmodified[b].append(a)
    
    @timed('expanding the graph')
    def expand(self, n: int):
        """
        Expand currently saved graph, increasing the highest covered number.
        
        This will restore all removed connections and vertices!
        """
        
        for i in range(self.size + 1, n + 1):
            self._connections_unmodified[i] = []
            
            if i in [1, 2, 4]:  # these numbers don't have a lower number that can be added to reach a perfect square
                continue
            
            sqrt_lowest = math.sqrt(i)
            # all numbers should start their search with the next highest square
            if sqrt_lowest % 1 == 0:
                sqrt_lowest = int(sqrt_lowest) + 1
                # … for perfect square this means simply the next perfect square
            else:
                sqrt_lowest = math.ceil(sqrt_lowest)
            sqrt_highest = math.floor(math.sqrt(2 * i - 1))
            for j in range(sqrt_lowest, sqrt_highest + 1):
                self._connection_add(i, j ** 2 - i)
        
        self.reset()
    
    def reset(self):
        # we're not using deepcopy here since it's several orders of magnitude slower than this
        self._connections = {}
        for k, v in self._connections_unmodified.items():
            self._connections[k] = v.copy()
    
    def get_connections(self, n: int) -> List[int]:
        assert n <= self.size
        assert self._connections[n] is not None
        
        return self._connections[n].copy()
    
    def connection_remove(self, a: int, b: int):
        """
        Temporarily remove a connection from the graph.
        """
        
        assert self._connections[a] is not None
        assert self._connections[b] is not None
        
        self._connections[a].remove(b)
        self._connections[b].remove(a)
    
    def connection_restore(self, a: int, b: int):
        """
        Restore a temporarily removed connection.
        """
        
        assert a in self._connections_unmodified[b], f'there is no connection from {a} to {b} in the unmodified graph!'
        assert b in self._connections_unmodified[a], f'there is no connection from {b} to {a} in the unmodified graph!'
        assert self._connections[a] is not None
        assert self._connections[b] is not None
        
        self._connections[a].append(b)
        self._connections[b].append(a)
    
    def sort_vertices(self, vertices: List[int], reverse: bool = False):
        return sorted(vertices, key=lambda v: len(self._connections[v]), reverse=reverse)
    
    @property
    def vertices(self) -> List[int]:
        return [vertex for vertex in self._connections.keys() if self._connections[vertex] is not None]
    
    def vertex_remove(self, n: int):
        assert self._connections[n] is not None
        
        for link in self._connections[n]:
            self._connections[link].remove(n)
        
        self._connections[n] = None
    
    def vertex_restore(self, n: int):
        assert self._connections[n] is None
        
        self._connections[n] = [link for link in self._connections_unmodified[n] if self._connections[link] is not None]
        for link in self._connections[n]:
            self._connections[link].append(n)
            
            
class DFSVertexQueue(Collection, Iterator):
    def __init__(self):
        self._queue: Dict[int, List[int]] = {}
    
    def __len__(self) -> int:
        return len(self._queue)

    def __iter__(self) -> 'DFSVertexQueue':
        return self
    
    def __next__(self):
        return self.pop()
    
    def __contains__(self, item: object) -> bool:
        return item in self._queue
    
    def pop(self) -> (int, int):
        current_vertex, current_connections = self._queue.popitem()
        
        if len(current_connections) == 0:
            return current_vertex, None
        else:
            next_connection = current_connections.pop()
            self.append(current_vertex, current_connections)
            
            return current_vertex, next_connection
    
    def append(self, vertex: int | None, connections: List[int]):
        assert vertex not in self._queue
        self._queue[vertex] = connections
    
    def popitem(self):
        self._queue.popitem()


class NoPossibleConnectionsError(ArithmeticError):
    pass


def find_sequence_depth_first(
        graph: Graph
) -> Sequence | bool:
    queue = DFSVertexQueue()
    queue.append(None, graph.sort_vertices(graph.vertices))  # using index None as entrypoint of our search
    path = []
    
    for vertex, next_vertex in queue:
        assert vertex is None and len(path) == 0 or vertex == path[-1]
        
        if next_vertex is None:
            graph.vertex_restore(vertex)
            path.pop()
            continue
        
        path.append(next_vertex)
        connected_vertices = graph.get_connections(next_vertex)
        connected_vertices = graph.sort_vertices(connected_vertices, reverse=True)
        graph.vertex_remove(next_vertex)
        queue.append(next_vertex, connected_vertices)
        
        if len(path) == graph.size:
            return path

    raise NoPossibleConnectionsError


def combine_sequences(s1: Sequence, s2: Sequence, graph: Graph) -> Sequence:
    if s1[0] in graph.get_connections(s2[0]):
        return s1[::-1] + s2
    elif s1[-1] in graph.get_connections(s2[0]):
        return s1 + s2
    elif s1[0] in graph.get_connections(s2[-1]):
        return s2 + s1
    elif s1[-1] in graph.get_connections(s2[-1]):
        return s1 + s2[::-1]
    else:
        raise NotCombinableError


def reconnect_sequences(s1: Sequence, s2: Sequence, graph: Graph) -> (Sequence, Sequence):
    connections_shuffled = graph.get_connections(s2[0])
    random.shuffle(connections_shuffled)
    for c in connections_shuffled:
        if c in s1[:-1]:
            index = s1.index(c)
            return s1[index + 1:], s1[:index + 1] + s2
    
    raise NotCombinableError


def extend_sequence(seq: Sequence, addition: int, graph: Graph) -> Sequence:
    """
    This is based on https://github.com/FluorineDog/square_sum
    """
    
    s1 = seq.copy()
    s2 = [addition]
    for i in range(10000):
        if i > 0 and i % 1000 == 0:
            logging.debug(f'trying to extend sequence (round {i}) of total length {len(s1) + len(s2)}')
        
        try:
            return combine_sequences(s1, s2, graph)
        except NotCombinableError:
            pass
        except TypeError as e:
            logging.error(f's1: {s1}')
            logging.error(f's2: {s2}')
            raise e
        
        try:
            s1, s2 = reconnect_sequences(s1, s2, graph)
        except NotCombinableError:
            try:
                s1, s2 = reconnect_sequences(s2, s1, graph)
            except NotCombinableError:
                logging.error('looks like we ran into a deadlock')
                logging.error(f'sequence 1: {s1}')
                logging.error(f'sequence 2: {s2}')
    
    logging.error('could not combine sequences, looks like we had a bad run')
    logging.error(f'original sequence: {seq}')
    logging.error(f'addition to the sequence: {addition}')
    logging.error(f's1: {s1}')
    logging.error(f's2: {s2}')


@timed('finding the sequence')
def find_sequence(n: int, dfs_only: bool = False) -> Sequence | bool:
    if dfs_only:
        graph = Graph(n)
        return find_sequence_depth_first(graph)
    
    graph = Graph(min(n, 40))
    
    try:
        seq = find_sequence_depth_first(graph)
    except NoPossibleConnectionsError:
        return False
    
    # TODO: remove used vertices in the graph when expanding it
    graph.expand(n)
    
    # TODO: the extension routine should use the ability of Graph to remove and restore connections
    for number in range(len(seq) + 1, n + 1):
        if number % 1000 == 0:
            logging.debug(f'expanding the sequence, currently at {number}')
        
        seq = extend_sequence(seq, number, graph)
    
    return seq


def print_sequence(sequence: List[int]):
    assert len(sequence) >= 1, 'The sequence has to contain at least one element'
    
    middle_str = f'[ {sequence[0]},'
    above_sum = ' ' * (len(middle_str) - 1)
    above_operand = ' ' * (len(middle_str) - 1)
    above_operand_addition = '/+\\'
    below_sum = ' ' * (len(middle_str) - 1)
    below_operand = ' ' * (len(middle_str) - 1)
    below_operand_addition = '\\+/'
    
    for i in range(1, len(sequence)):
        middle_addition = f' {sequence[i]},'
        middle_str += middle_addition
        if i % 2:
            above_sum_addition = f'{sequence[i - 1] + sequence[i]:^{len(middle_addition)}}'
            above_sum += above_sum_addition
            above_operand += f'{above_operand_addition:^{len(middle_addition)}}'
            below_sum += ' ' * len(middle_addition)
            below_operand += ' ' * len(middle_addition)
        else:
            above_sum += ' ' * len(middle_addition)
            above_operand += ' ' * len(middle_addition)
            below_sum_addition = f'{sequence[i - 1] + sequence[i]:<{len(middle_addition)}}'
            below_sum += below_sum_addition
            below_operand += f'{below_operand_addition:<{len(middle_addition)}}'
    
    middle_str = middle_str[:-1]
    middle_str += ' ]'
    
    logging.info('\n' + above_sum + '\n' + above_operand + '\n' + middle_str + '\n' + below_operand + '\n' + below_sum)


def print_evaluation_of_sequences(sequences: SequenceList, short: bool = False):
    if sequences:
        logging.info(f'found {len(sequences)} sequences of length {len(sequences[0])}')
        for s in sequences:
            if short:
                logging.info(s)
            else:
                print_sequence(s)
    else:
        logging.info('no sequences found')


def verify_sequence(seq: Sequence):
    for i in range(1, len(seq)):
        assert (seq[i] + seq[i - 1]) ** .5 % 1 == 0, f'Sequence invalid: {seq}'
    
    logging.info(f'the following sequence of length {len(seq):,} has been validated: {seq}')


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    
    n = 20000
    
    # TODO: improve the variant using expansion as well, currently only the depth first search is pretty well optimized
    s = find_sequence(n, dfs_only=True)
    verify_sequence(s)
    # print_evaluation_of_sequences([s], short=True)
