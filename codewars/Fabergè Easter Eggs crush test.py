"""
for details see
 - https://www.codewars.com/kata/54cb771c9b30e8b5250011d4/python
 - https://www.codewars.com/kata/5976c5a5cd933a7bbd000029/python
"""

import math


def height(eggs: int, tries: int):
    if eggs <= 0 or tries <= 0:
        return 0
    if eggs == 1:
        # print(f'({eggs}, {tries}) -> {tries}')
        return tries
    elif tries == 1:
        # print(f'({eggs}, {tries}) -> 1')
        return 1
    elif eggs > tries:
        return height(tries, tries)
    elif eggs <= tries:
        return nCr(tries, eggs)
        
        if eggs == 2:
            return nCr(tries + eggs - 1, eggs)
        
        floor = 0
        for t in range(tries - 1, 0, -1):
            if (eggs - 1) > t:
                floor += height(t, t) + 1
            else:
                floor += height(eggs - 1, t) + 1
        # print(f'({eggs}, {tries}) -> {floor + 1}')
        return floor + 1
    else:
        raise NotImplementedError()
    
    
def nCr(n, r):
    try:
        return math.factorial(n) // math.factorial(r) // math.factorial(n - r)
    except:
        return 0


if __name__ == '__main__':
    print('(2, 14):', height(2, 14))
    print('(7, 20):', height(7, 20))
    print('(3, 3):', height(3, 3))
    print('(3, 4):', height(3, 4))
    print('(3, 5):', height(3, 5))
    print('(3, 6):', height(3, 6))
    print('(3, 7):', height(3, 7))
    print('(3, 8):', height(3, 8))
    print('(3, 9):', height(3, 9))
    print('(3, 10):', height(3, 10))
    print('(4, 4):', height(4, 4))
    print('(4, 5):', height(4, 5))
    print('(4, 6):', height(4, 6))
    print('(4, 7):', height(4, 7))
    print('(4, 8):', height(4, 8))
    print('(4, 9):', height(4, 9))
    print('(4, 10):', height(4, 10))
    print('(4, 11):', height(4, 11))
    print('(4, 12):', height(4, 12))
    print('(4, 13):', height(4, 13))
    print('(4, 14):', height(4, 14))
    print('(4, 15):', height(4, 15))
    print('(7, 500):', height(7, 500))
    
    floor = 0
    for i in range(14):
        j = i + 1
        floor += (j - 1) * 2
    print(floor)
