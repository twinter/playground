def turing_machine_task():
    b0 = 6  # input / output
    b1 = None  # number to add
    b2 = None  # remaining additions
    b3 = None  # remaining multiplications
    
    b3 = b0
    b0 = 1
    
    while b3 > 0:
        b3 -= 1
        
        b1 = b0
        b2 = b3
        
        while b2 > 0:
            b0 += b1
            b2 -= 1
    
    print(b0)
    
    
def aufgabe4_1a():
    # factorial function
    x1 = 6  # n
    x2 = 1
    for i in range(x1):
        x3 = x2
        x1 = x1 - 1
        for j in range(x1):
            x2 += x3
    print(x2)
    
    
def aufgabe4_1c():
    # fibonacci sequence
    x1 = 6  # input
    x2 = 0
    x3 = 1
    x4 = x1
    x1 = 0
    for i in range(x4):
        x1 = x2 + x3
        x3 = x2
        x2 = x1
    print(x1)


if __name__ == '__main__':
    aufgabe4_1c()
