import itertools
from typing import Tuple


def generate_all_combinations(max_value: int, rounds: int) -> Tuple[Tuple[int]]:
    dice_numbers = tuple(range(1, max_value + 1))
    dice_numbers_combined = (dice_numbers,) * rounds
    combinations = itertools.product(*dice_numbers_combined)
    return tuple(combinations)


def get_average_of_set(set: Tuple, exclude_lowest_n: int = 0):
    assert exclude_lowest_n >= 0
    
    set = sorted(set, reverse=True)
    included_items = len(set) - exclude_lowest_n
    included_items = max(0, included_items)
    return sum(set[:included_items+1]) / included_items


if __name__ == '__main__':
    combinations = generate_all_combinations(max_value=6, rounds=3)
    averages = []
    for item in combinations:
        averages.append(get_average_of_set(item, exclude_lowest_n=1))
    avg = get_average_of_set(averages)
    print(avg)
